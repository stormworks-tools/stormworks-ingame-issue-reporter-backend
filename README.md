# Intro

This is a webserver that can be called from inside Stormworks Lua scripts via async.httpGet() that allow you to interact with the bug reporter on http://mcro.org/issues/1/

The definition of the API can be seen [below](#api)


## Stormworks Vehicle that uses this API:

https://steamcommunity.com/sharedfiles/filedetails/?id=2277416147

<br>
<br>
<br>

# Usage (View only)
This allows you to view issues, but not create new ones.

## Variant 1 : Executables
This requires no installation or any setup. Download this repository and run `stormworks-ingame-bug-reporter-win.exe`.
https://gitlab.com/stormworks-tools/stormworks-ingame-issue-reporter-backend/-/archive/master/stormworks-ingame-issue-reporter-backend-master.zip

Windows will open some warning errors when you try to execute this file, because obviously i have not signed the executable.
If you do not trust me, then use it via nodejs (see below) or build the executable from the source code by yourself via nodejs and pkg (see [below](#how-to-create-your-own-executable)).

## Variant 2 : nodejs

run `node app.js`

<br>
<br>
<br>

# Usage (View and Create Issues)
This allows you to view issues and create new ones.

This requires that you find out your steamid and a special code that the stormworks generates.

<br>

## Variant 1 : Using Fiddler to record HTTP data
You should only do this if you already have Fiddler installed and know how Fiddler works

* Press the button "Report a Bug" in the game
* select the http request that was made
* the url will look similar to this: `mcro.org/users/login_steam?user=123456789_97ba623f3&code=97ba623f361e62cd2&ste...`
* your steam id is the part between `?user` and the next `_`. So in the above example it's `123456789`
* your steam code is the part between `&code` and the next `_`. So in the above example it's `97ba623f361e62cd2`

<br>

## Variant 2 : Cancel Page Loading
In order to work you have to be fast. Before you start, read through all the steps!

* Press the button "Report a Bug" in the game
* As soon as your browser opens a new tab, cancel the page loading (the cross in the top left corner next to the url bar)

<img src="readme_img1.png">

* the url will look similar to this: `mcro.org/users/login_steam?user=123456789_97ba623f3&code=97ba623f361e62cd2&ste...`
* your steam id is the part between `?user` and the next `_`. So in the above example it's `123456789`
* your steam code is the part between `&code` and the next `_`. So in the above example it's `97ba623f361e62cd2`

If you cannot reach the button fast enough and the page loads (showing the official mcro.org website) try Variant 3

<br>

## Variant 3 : Screenshot
* Press the button "Report a Bug" in the game
* As soon as your browser opens a new tab, make a screenshot (by pressing the button called `Print`, `PrtSc`, `Druck`, see google for examples: https://www.google.com/search?q=print+screen+button&tbm=isch)

<img src="readme_img1.png">

* the url will look similar to this: `mcro.org/users/login_steam?user=123456789_97ba623f3&code=97ba623f361e62cd2&ste...`
* your steam id is the part between `?user` and the next `_`. So in the above example it's `123456789`
* your steam code is the part between `&code` and the next `_`. So in the above example it's `97ba623f361e62cd2`

<br>

## Startup with steam authentication

### Variant 1 : Executables
Starting with steam authentication using the steam id and steam code from the example above

#### -> Windows
**via CMD**

run `stormworks-ingame-bug-reporter-win.exe steam-id=123456789 steam-code=97ba623f361e62cd2`

**by clicking a bat file**
* edit the file `start_exe.bat`
* replace the steam id and code with your own values
* save
* doubleclick the file to run

#### -> Linux / MacOS
**i only tested windows, but you should be able to use the linux and macos executable pretty similar**

<br>

### Variant 2 : nodejs
Starting with steam authentication using the steam id and steam code from the example above

run `node app.js steam-id=123456789 steam-code=97ba623f361e62cd2`

<br>
<br>
<br>

# How to create your own executable

## Setup pkg
run `npm install -g pkg`

(Source / Further reading: https://dev.to/jochemstoel/bundle-your-node-app-to-a-single-executable-for-windows-linux-and-osx-2c89)

## Build with pkg
run `pkg .`

<br>
<br>
<br>

# API
* All your requests must be sent on port 4242 and to /script
* This webserver will always respond with json

**Example lua script code:**

```lua
async.httpGet(4242, "/script?type=latest")

function httpReply(port, request, response)
  data = json.parse(response)
end

function onDraw()
  if data then
    screen.setColor(255,255,255)
    screen.drawTextBox(0,0,screen.getWidth(),screen.getHeight(), data)
  else
    screen.setColor(255,0,0)
    screen.drawTextBox(0,0,screen.getWidth(),screen.getHeight(), "Loading ...", 0,0)
  end
end
```

## Error handling

Whenever an error happens, you will receive a json like this:
```json
{
  "error": "This is an error message"
}
```


These different types are available:

## type=latest

Show latest issues

**Other parameters:**
none

```json
{
  "type": "latest",
  "entries": [
    {
      "name": "Name of the issue",
      "createdby": "Username of the author",
      "id": "1234"
    },
    ...
  ]
}
```

## type=search

Search for an issue

**Other parameters:**

| param name | description | example |
| --- | --- | ---|
| q | what you are searching for (min length: 4 chars) | electric relay |


```json
{
  "type": "search",
  "query": "Your query text",
  "entries": [
    {
      "name": "Name of the issue",
      "createdby": "Username of the author",
      "id": "1234"
    },
    ...
  ]
}
```

## type=detail

Search for an issue

**Other parameters:**

| param name | description | example |
| --- | --- | --- |
| id | id of the issue you want to retreive | 21635 |


```json
{
  "type": "detail",
  "name": "Name of the issue",
  "description": "Description of the issue",
  "id": "1234",
  "comments": [
    {
      "content": "This is a comment",
      "createdby": "Username of the author"
    },
    ...
  ]
}
```

## type=report

Create a new issue

**Other parameters:**

| param name | description | example |
| --- | --- | --- |
| title | name of the new issue you want to create (min length: 5 chars) | Electric Relays are broken |
| description | detailed description of your problem (min length: 4 chars) | When connecting to a battery ... |
| kind | "defect" => creates a bug report, "feature" => creates a feature request | defect |


```json
{
  "type": "report-success",
  "kind": "defect"
}
```

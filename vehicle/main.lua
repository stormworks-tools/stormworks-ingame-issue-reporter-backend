--[[ json.lua
A compact pure-Lua JSON library.
The main functions are: json.stringify, json.parse.
## json.stringify:
This expects the following to be true of any tables being encoded:
 * They only have string or number keys. Number keys must be represented as
   strings in json; this is part of the json spec.
 * They are not recursive. Such a structure cannot be specified in json.
A Lua table is considered to be an array if and only if its set of keys is a
consecutive sequence of positive integers starting at 1. Arrays are encoded like
so: `[2, 3, false, "hi"]`. Any other type of Lua table is encoded as a json
object, encoded like so: `{"key1": 2, "key2": false}`.
Because the Lua nil value cannot be a key, and as a table value is considerd
equivalent to a missing key, there is no way to express the json "null" value in
a Lua table. The only way this will output "null" is if your entire input obj is
nil itself.
An empty Lua table, {}, could be considered either a json object or array -
it's an ambiguous edge case. We choose to treat this as an object as it is the
more general type.
To be clear, none of the above considerations is a limitation of this code.
Rather, it is what we get when we completely observe the json specification for
as arbitrary a Lua object as json is capable of expressing.
## json.parse:
This function parses json, with the exception that it does not pay attention to
\u-escaped unicode code points in strings.
It is difficult for Lua to return null as a value. In order to prevent the loss
of keys with a null value in a json string, this function uses the one-off
table value json.null (which is just an empty table) to indicate null values.
This way you can check if a value is null with the conditional
`val == json.null`.
If you have control over the data and are using Lua, I would recommend just
avoiding null values in your data to begin with.
--]]

local json = {}

-- Internal functions.

-- Returns pos, did_find; there are two cases:
-- 1. Delimiter found: pos = pos after leading space + delim; did_find = true.
-- 2. Delimiter not found: pos = pos after leading space;     did_find = false.
-- This throws an error if err_if_missing is true and the delim is not found.
local function skip_delim(str, pos, delim, err_if_missing)
  pos = pos + #str:match('^%s*', pos)
  if str:sub(pos, pos) ~= delim then
    if err_if_missing then
      error('t ' .. delim .. ' ' .. pos)
    end
    return pos, F
  end
  return pos + 1, T
end

-- Expects the given pos to be the first character after the opening quote.
-- Returns val, pos; the returned pos is after the closing quote character.
local function parse_str_val(str, pos, val)
  val = val or ''
  local early_end_error = 'z'
  if pos > #str then error(early_end_error) end
  local c = str:sub(pos, pos)
  if c == '"'  then return val, pos + 1 end
  if c ~= '\\' then return parse_str_val(str, pos + 1, val .. c) end
  -- We must have a \ character.
  local esc_map = {b = '\b', f = '\f', n = '\n', r = '\r', t = '\t'}
  local nextc = str:sub(pos + 1, pos + 1)
  if not nextc then error(early_end_error) end
  return parse_str_val(str, pos + 2, val .. (esc_map[nextc] or nextc))
end

-- Returns val, pos; the returned pos is after the number's final character.
local function parse_num_val(str, pos)
  local num_str = str:match('^-?%d+%.?%d*[eE]?[+-]?%d*', pos)
  local val = tonumber(num_str)
  if not val then return F, 'y ' .. pos end
  return val, pos + #num_str
end

function json.parse(str, pos, end_delim)
  pos = pos or 1
  if pos > #str then return F, 'x' end
  local pos = pos + #str:match('^%s*', pos)  -- Skip whitespace.
  local first = str:sub(pos, pos)
  if first == '{' then  -- Parse an object.
    local obj, key, delim_found = {}, T, T
    pos = pos + 1
    while T do
      key, pos = json.parse(str, pos, '}')
      if key == nil then return obj, pos end
      if not delim_found then return F, 'w' end
      pos = skip_delim(str, pos, ':', T)  -- true -> error if missing.
      obj[key], pos = json.parse(str, pos)
      pos, delim_found = skip_delim(str, pos, ',')
    end
  elseif first == '[' then  -- Parse an array.
    local arr, val, delim_found = {}, T, T
    pos = pos + 1
    while T do
      val, pos = json.parse(str, pos, ']')
      if val == nil then return arr, pos end
      if not delim_found then return F, 'v' end
      arr[#arr + 1] = val
      pos, delim_found = skip_delim(str, pos, ',')
    end
  elseif first == '"' then  -- Parse a string.
    return parse_str_val(str, pos + 1)
  elseif first == '-' or first:match('%d') then  -- Parse a number.
    return parse_num_val(str, pos)
  elseif first == end_delim then  -- End of an object or array.
    return nil, pos + 1
  else  -- Parse true, false, or null.
    local literals = {['true'] = T, ['false'] = F, ['null'] = {}}
    for lit_str, lit_val in pairs(literals) do
      local lit_end = pos + #lit_str - 1
      if str:sub(pos, lit_end) == lit_str then return lit_val, lit_end + 1 end
    end
    error('u ' .. pos .. ': ' .. str:sub(pos, pos + 10))
  end
end

--[[

	Lua parse error codes:
	
	t = Expected [del] near
	u = Invalid syntax start at position 
	v = Comma missing between array items
	w = Comma missing between object items
	x = Reached unexpected end of input
	y = Error parsing number at position
	z = End of input found while parsing
]]

function error(msg)
	data = {error=msg}
end


-- The actual code
L = "latest"
s = "search"
F = false
T = true
iN = input.getNumber
iB = input.getBool

data = nil
ticks = 0
function get(typ, more)
	if data == F then
		return
	end
	_typ = typ
	_more = more
	ticks = 0
	--print("getting", typ, more)
	data = F
	async.httpGet(4242,"/script?type=" .. typ ..  (more or ""))
end

function httpReply(port,url,response)
	--print("got", url)
	data = json.parse(response)
end

get(L)


iP = F
view = 0 --0 == i am showing, 1 == search is showing, 2 == report is showing
function onTick()
	ticks = ticks + 1
	if (not data or not data.type) and ticks > 300 then
		data = nil
		get(_typ,_more) -- automatic reload
	end
	
	
	scroll = iN(32)
	pX = iN(3)
	pY = iN(4)
	if iB(1) and not iP then
		if view == 0 and touch(28,10,36,9) then
			data = nil
			get(_typ,_more)
		elseif data then
			if touch(0,10,26,9) then
				view = 0
				get(L)
			elseif touch(SW-36,10,36,9) then
				view = 1
			elseif touch(SW-56-2-36,10,56,9) then
				view = 2
			elseif view == 0 and (data.type == L or data.type == s) then
				for i, e in ipairs(data.entries) do
					if touch(e.x,e.y,SW-e.x*2,20) then-- 20 = height per entry
						get("detail", "&id=" .. e.id)
						break
					end
				end
			end
		end
	end
	iP = iB(1)
	
	output.setNumber(1, view)
	if iB(31) then
		-- go back to my view and search (sent by search script)
		view = 0
		
		q = ""
		for i=10,30 do
			if iN(i) > 0 then
				q = q .. string.char(iN(i))
			end
		end
		get(s, "&q=" .. q)
	elseif iB(32) then
		-- go back to my view and look at latest
		view = 0
		get(L)
	end
	
	output.setBool(1, not data)
end

function touch(x,y,w,h)
	return pX >= x and pY >= y and pX <= x + w and pY <= y + h
end

function onDraw()
	setColor = screen.setColor
	drawTextBox = screen.drawTextBox
	drawRectF = screen.drawRectF
	drawText = screen.drawText
	
	propertyText = property.getText
	
	
	SW, SH = screen.getWidth(), screen.getHeight()

	if data then
		setBackgroundColor()
		drawRectF(0,0,SW,SH)
		
		if view == 1 then
			drawNav(s)
		elseif view == 2 then
			drawNav("add/1/defect")
		else
			if data.error then
				drawStatus(data.error)
				drawNav("error")
			elseif data.type == L then
				drawEntries(data.entries, 9, 35 - scroll)
				drawHeading(propertyText("1"))
				drawNav("1")
			elseif data.type == "detail" then
				drawComments(data.comments, 15, 3 + drawMultilineText(data.description, 5, 47 - scroll, setIssueDescriptionBackgroundColor))
				setTextColor()
				drawText(5,37 - scroll, "Created by " .. data.user)
				drawHeading(data.name)
				drawNav("view_issue/" .. data.id)
			elseif data.type == s then
				drawEntries(data.entries, 9, 35 - scroll)
				drawHeading("Result")
				drawNav(s .. "/1?search_query=" .. data.query)
			end
		end
		drawButton(0,10,"HOME")
		drawButton(SW-36,10,s)
		drawButton(SW-56-2-36,10,"REPORT BUG")
	else
		drawStatus("loading ... " .. math.floor(ticks/60) .. "s")
	end
	if view == 0 then
		drawButton(28,10,"RELOAD")
	end
end


function drawButton(x,y,l)
	drawTextWithBackground(x,y,#l * 5 + 6,9, l, setLinkColor,setTextColor)
end

-- l = text, bf = function to color background, tf = function to color text
function drawTextWithBackground(x,y,w,h,l,bf,tf)
	bf()
	drawRectF(x,y,w,h)
	tf()
	drawTextBox(x,y,w,h, l, 0,0)
end

-- draw a black background with white text
function drawStatus(m)
	drawTextWithBackground(0,20,SW,SH,m,function() setColor(120,120,120) end,setTextColor)
end

function drawNav(m)
	drawTextWithBackground(0,0,SW,10, propertyText("2") .. m, setTextColor,setMediumBackgroundColor)
end

function drawHeading(_)
	setTextColor()
	drawTextBox(5,25-scroll,SW-10,5, _, 0,0)
end

-- x = left padding, y = y offset
function drawEntries(E, x, o)
	for i, e in ipairs(E) do
		y = o + (i-1) * 20 -- 20 = height per entry
		E[i].x = x
		E[i].y = y
		setTextColor()
		drawRectF(x,y,SW - x*2, 1)
		drawText(x,y + 3, limit(e.name, (SW - x*2) / 5))
		setLinkColor()
		drawText(x,y + 11, limit(e.createdby, (SW - x*2) / 5))
	end
end

-- x = left padding, o = y offset
function drawComments(C, x, o)
	for i, c in ipairs(C) do
		o = drawMultilineText(c.content, x, o, setMediumBackgroundColor)
		o = o + 1
		setLinkColor()
		drawText(x,o + 1, limit(c.createdby, (SW - x*2) / 5))
		o = o + 10
	end
end

-- t=text, x = left padding, o = y offset, bf = function to set color for background
-- returns offset + height of multiline text
function drawMultilineText(t, x, o, bf)
	pos = 1
	while pos < #t do
		l = limit(t, (SW - x*2) / 5, pos)
		nl = string.find(l, "\n")
		if nl then
			l = string.sub(l, 1, nl)
		end
		if bf then
			bf()
			drawRectF(x,o,SW - x*2, 9)
		end
		setTextColor()
		drawText(x+2,o+2, l)
		
		o = o + 9
		
		pos = pos + #l
	end
	return o
end

-- limits string to a max length l, o = cut of chars at the beginning (defaults to 1 = no cutoff)
function limit(s, l, o)
	return string.sub(s, o or 1, (o or 1) + math.floor(l - 1))
end

function setTextColor()
	setColor(0,0,0)
end

function setBackgroundColor()
	setColor(200,200,200)
end

function setIssueDescriptionBackgroundColor()
	setColor(120,80,80)
end

function setMediumBackgroundColor()
	setColor(80,80,80)
end

function setLinkColor()
	setColor(20,60,150)
end

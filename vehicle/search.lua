MY_VIEW = 1

	
	
currentMessage = ""

writeMode = false

sendButton = nil
inp = nil

inpWasPressed = false

sendQuery = false
sendingTimeout = 0

waskeyboardShown = false
function onTick()
	selectedView = input.getNumber(1)
	
	output.setBool(30, false)
	output.setBool(29, false)
	
	if selectedView == MY_VIEW then
		
		keyboard = parseKeyboard(input.getNumber(32))

		if waskeyboardShown and not keyboard.keyboardShown then
			writeMode = false
		end
		waskeyboardShown = keyboard.keyboardShown
		
		if writeMode and keyboard.key > 0 then
			if keyboard.CONFIRM then
				writeMode = false
				
				-- close keyboard
				output.setBool(30, true)
			elseif keyboard.DEL then
				currentMessage = string.sub(currentMessage, 1, -2)
			else
				currentMessage = currentMessage .. keyboard.char
			end
			currentMessage = string.sub(currentMessage, 1, 21) -- max 21 chars
		end
		
		if sendingTimeout > 0 then
			sendingTimeout = sendingTimeout - 1
		end
		
		if not writeMode and sendingTimeout <= 0 then
			if sendButton and not keyboard.keyboardShown and keyboard.click and touch(sendButton) then 
				sendQuery = true
				sendingTimeout = 60
			elseif inp and not keyboard.keyboardShown and keyboard.click and touch(inp) then 
				inpWasPressed = true
			end
		end
		if inpWasPressed and not keyboard.click then
			inpWasPressed = false
			writeMode = true
			
			--request keyboard
			output.setBool(29, true)
		end
	end
	
	
	-- signal main script to switch to latest view
	-- output.setBool(32, true)
	
	output.setBool(31, false)
	if sendQuery then
		sendQuery = false
		
		for i=1,21 do -- channels 10-30
			local char = string.sub(currentMessage, i, i)
			if char then
				output.setNumber(9 + i, string.byte(char))
			end
		end
		
		-- signal main script to switch to search view
		output.setBool(31, true)
	end
end

function touch(o)
	return keyboard.x >= o.x and keyboard.y >= o.y and keyboard.x <= o.x + o.w and keyboard.y <= o.y + o.h
end

-- bit 1 = type
-- bit 2 = keyboard is shown
-- type==true
-- bits 3-13 = x
-- bits 14-24 = y
-- type==false
-- bits 2-8 = charcode
function parseKeyboard(numb)
	local fl = math.floor(numb)
	local type = (numb & 1) == 1
	local keyboardShown = (numb & 2) == 2
	
	local ret = {
		type = type,
		keyboardShown = keyboardShown,
		click = false,
		x = 0,
		y = 0,
		key = 0,
		char = ""
	}
	if type then
		ret.click=true
		ret.x=(fl >> 2) & 2047
		ret.y=(fl >> 13) & 2047
	else
		local charc = ((fl >> 2) & 127)
			ret.key = charc
		if charc > 31 and charc < 127 then
			ret.char = string.char(ret.key)
		else
			ret.key = charc
			if charc == 4 then
				ret.CONFIRM = true
			elseif charc == 127 then
				ret.DEL = true
			end
		end
	end	
	return ret
end


SW = 0
SH = 0

function onDraw()
	S=screen
	SW=S.getWidth()
	SH=S.getHeight()
	
	setColor = S.setColor
	
	
	if selectedView == MY_VIEW then
				
		sendButton = {x=SW-20-35,y=35,w=35,h=9}
		
		inp = {x=20,y=35,w=SW-40-40,h=9}
		
		setColor(0,0,0)
		S.drawText(inp.x,inp.y-9,"Searchstring")
		setColor(40,40,40)
		S.drawRect(inp.x,inp.y,inp.w,inp.h)
		setColor(0,0,0)
		S.drawTextBox(inp.x+2,inp.y,inp.w-4,inp.h, currentMessage, -1,0)
		
		if not keyboard.keyboardShown then
			setColor(20,60,150)
			S.drawRectF(sendButton.x, sendButton.y, sendButton.w, sendButton.h)
			setColor(255,255,255)
			S.drawTextBox(sendButton.x, sendButton.y, sendButton.w, sendButton.h, "SEARCH", 0, 0)
		end
	end
end
--[[ json.lua
A compact pure-Lua JSON library.
The main functions are: json.stringify, json.parse.
## json.stringify:
This expects the following to be true of any tables being encoded:
 * They only have string or number keys. Number keys must be represented as
   strings in json; this is part of the json spec.
 * They are not recursive. Such a structure cannot be specified in json.
A Lua table is considered to be an array if and only if its set of keys is a
consecutive sequence of positive integers starting at 1. Arrays are encoded like
so: `[2, 3, false, "hi"]`. Any other type of Lua table is encoded as a json
object, encoded like so: `{"key1": 2, "key2": false}`.
Because the Lua nil value cannot be a key, and as a table value is considerd
equivalent to a missing key, there is no way to express the json "null" value in
a Lua table. The only way this will output "null" is if your entire input obj is
nil itself.
An empty Lua table, {}, could be considered either a json object or array -
it's an ambiguous edge case. We choose to treat this as an object as it is the
more general type.
To be clear, none of the above considerations is a limitation of this code.
Rather, it is what we get when we completely observe the json specification for
as arbitrary a Lua object as json is capable of expressing.
## json.parse:
This function parses json, with the exception that it does not pay attention to
\u-escaped unicode code points in strings.
It is difficult for Lua to return null as a value. In order to prevent the loss
of keys with a null value in a json string, this function uses the one-off
table value json.null (which is just an empty table) to indicate null values.
This way you can check if a value is null with the conditional
`val == json.null`.
If you have control over the data and are using Lua, I would recommend just
avoiding null values in your data to begin with.
--]]

local json = {}

-- Internal functions.

-- Returns pos, did_find; there are two cases:
-- 1. Delimiter found: pos = pos after leading space + delim; did_find = true.
-- 2. Delimiter not found: pos = pos after leading space;     did_find = false.
-- This throws an error if err_if_missing is true and the delim is not found.
local function skip_delim(str, pos, delim, err_if_missing)
  pos = pos + #str:match('^%s*', pos)
  if str:sub(pos, pos) ~= delim then
    if err_if_missing then
      error('t ' .. delim .. ' ' .. pos)
    end
    return pos, F
  end
  return pos + 1, T
end

-- Expects the given pos to be the first character after the opening quote.
-- Returns val, pos; the returned pos is after the closing quote character.
local function parse_str_val(str, pos, val)
  val = val or ''
  local early_end_error = 'z'
  if pos > #str then error(early_end_error) end
  local c = str:sub(pos, pos)
  if c == '"'  then return val, pos + 1 end
  if c ~= '\\' then return parse_str_val(str, pos + 1, val .. c) end
  -- We must have a \ character.
  local esc_map = {b = '\b', f = '\f', n = '\n', r = '\r', t = '\t'}
  local nextc = str:sub(pos + 1, pos + 1)
  if not nextc then error(early_end_error) end
  return parse_str_val(str, pos + 2, val .. (esc_map[nextc] or nextc))
end

-- Returns val, pos; the returned pos is after the number's final character.
local function parse_num_val(str, pos)
  local num_str = str:match('^-?%d+%.?%d*[eE]?[+-]?%d*', pos)
  local val = tonumber(num_str)
  if not val then return F, 'y ' .. pos end
  return val, pos + #num_str
end

function json.parse(str, pos, end_delim)
  pos = pos or 1
  if pos > #str then return F, 'x' end
  local pos = pos + #str:match('^%s*', pos)  -- Skip whitespace.
  local first = str:sub(pos, pos)
  if first == '{' then  -- Parse an object.
    local obj, key, delim_found = {}, T, T
    pos = pos + 1
    while T do
      key, pos = json.parse(str, pos, '}')
      if key == nil then return obj, pos end
      if not delim_found then return F, 'w' end
      pos = skip_delim(str, pos, ':', T)  -- true -> error if missing.
      obj[key], pos = json.parse(str, pos)
      pos, delim_found = skip_delim(str, pos, ',')
    end
  elseif first == '[' then  -- Parse an array.
    local arr, val, delim_found = {}, T, T
    pos = pos + 1
    while T do
      val, pos = json.parse(str, pos, ']')
      if val == nil then return arr, pos end
      if not delim_found then return F, 'v' end
      arr[#arr + 1] = val
      pos, delim_found = skip_delim(str, pos, ',')
    end
  elseif first == '"' then  -- Parse a string.
    return parse_str_val(str, pos + 1)
  elseif first == '-' or first:match('%d') then  -- Parse a number.
    return parse_num_val(str, pos)
  elseif first == end_delim then  -- End of an object or array.
    return nil, pos + 1
  else  -- Parse true, false, or null.
    local literals = {['true'] = T, ['false'] = F, ['null'] = {}}
    for lit_str, lit_val in pairs(literals) do
      local lit_end = pos + #lit_str - 1
      if str:sub(pos, lit_end) == lit_str then return lit_val, lit_end + 1 end
    end
    error('u ' .. pos .. ': ' .. str:sub(pos, pos + 10))
  end
end

--[[

	Lua parse error codes:
	
	t = Expected [del] near
	u = Invalid syntax start at position 
	v = Comma missing between array items
	w = Comma missing between object items
	x = Reached unexpected end of input
	y = Error parsing number at position
	z = End of input found while parsing
]]

function error(msg)
	data = {error=msg}
end


-- The actual code
T = true
F = false

MY_VIEW = 2

	
	
currentMessage = {"",""}

writeMode = false

activeInput = 1

sendButton = nil
inp1 = nil
inp2 = nil

inp1WasPressed = false
inp2WasPressed = false

sendQuery = false
isWaitingForResponse = false
signalToSwitch = false
errorMessage = false

waskS = false
function onTick()
	selectedView = input.getNumber(1)
	
	output.setBool(30, false)
	output.setBool(29, false)
	
	if selectedView == MY_VIEW then
		
		keyboard = parseKeyboard(input.getNumber(32))
		
		if not isWaitingForResponse then
			
			if waskS and not keyboard.kS then
				writeMode = false
			end
			waskS = keyboard.kS
			
			if writeMode and keyboard.key > 0 then
				if keyboard.CONFIRM then
					writeMode = false
					
					-- close keyboard
					output.setBool(30, true)
				elseif keyboard.DEL then
					currentMessage[activeInput] = string.sub(currentMessage[activeInput], 1, -2)
				else
					currentMessage[activeInput] = currentMessage[activeInput] .. keyboard.char
				end
				currentMessage[activeInput] = string.sub(currentMessage[activeInput], 1, activeInput == 1 and 30 or 999) -- max chars
			end
			
			if not writeMode then
				if sendButton and not keyboard.kS and keyboard.click and touch(sendButton) then 
					sendQuery = true
				elseif inp1 and not keyboard.kS and keyboard.click and touch(inp1) then 
					inp1WasPressed = true
				elseif inp2 and not keyboard.kS and keyboard.click and touch(inp2) then 
					inp2WasPressed = true
				end
			end
			if inp1WasPressed and not keyboard.click then
				inp1WasPressed = false
				writeMode = true
				activeInput = 1
				
				--request keyboard
				output.setBool(29, true)
			elseif inp2WasPressed and not keyboard.click then
				inp2WasPressed = false
				writeMode = true
				activeInput = 2
				
				--request keyboard
				output.setBool(29, true)
			end
		end
	end
	
	
	output.setBool(32, false)
	if sendQuery then
		sendQuery = false
		
		isWaitingForResponse = true
		async.httpGet(4242, "/script?type=report&kind=defect&title=" .. encode(currentMessage[1]) .. "&description=" .. encode(currentMessage[2]))
	end
	
	if signalToSwitch then
		signalToSwitch = false
		-- signal main script to switch to latest view
		output.setBool(32, true)
	end
end

function httpReply(port, url, response_body)
	isWaitingForResponse = false
	
	data = json.parse(response_body)
	
	if data and data.error then
		errorMessage = data.error
	else -- ignore other errors like timeouts
		signalToSwitch = true
	end
end

function encode(str)
	chars = ""
	for i=1,string.len(str) do
		c = string.sub(str,i,i)
		b = string.byte(c)
		if b < 33 or b == 96 or b > 126 then
			c = "?"
		end
		
		if c == "&" then
			c = "%26"
		elseif c == " " then
			c = "%20"
		elseif c == "=" then
			c = "%3D"
		elseif c == "?" then
			c = "%3F"
		end
		chars = chars .. c
	end
	return chars
end

function touch(o)
	return keyboard.x >= o.x and keyboard.y >= o.y and keyboard.x <= o.x + o.w and keyboard.y <= o.y + o.h
end

-- bit 1 = type
-- bit 2 = keyboard is shown
-- type==true
-- bits 3-13 = x
-- bits 14-24 = y
-- type==false
-- bits 2-8 = charcode
function parseKeyboard(numb)
	local fl = math.floor(numb)
	local type = (numb & 1) == 1
	local kS = (numb & 2) == 2
	
	local ret = {
		type = type,
		kS = kS,
		click = false,
		x = 0,
		y = 0,
		key = 0,
		char = ""
	}
	if type then
		ret.click=true
		ret.x=(fl >> 2) & 2047
		ret.y=(fl >> 13) & 2047
	else
		local charc = ((fl >> 2) & 127)
			ret.key = charc
		if charc > 31 and charc < 127 then
			ret.char = string.char(ret.key)
		else
			ret.key = charc
			if charc == 4 then
				ret.CONFIRM = true
			elseif charc == 127 then
				ret.DEL = true
			end
		end
	end	
	return ret
end


SW = 0
SH = 0

function onDraw()
	S=screen
	SW=S.getWidth()
	SH=S.getHeight()
	
	setColor = S.setColor
	
	
	if selectedView == MY_VIEW then
		
		off = 0
		if keyboard.kS then
			if activeInput == 2 then
				off = - (25 + math.floor(string.len(currentMessage[2]) * 5 / (SW - 40 - 40)) * 7 ) -- push upwards for every line in text
			else
				off = -10
			end
		end
		
		sendButton = {x=SW-20-35,y=30,w=35,h=9}
		
		inp1 = {x=20,y=30 + off,w=SW-40-40,h=9}
		inp2 = {x=20,y=55 + off,w=SW-40-40,h=60}
		
		
		if isWaitingForResponse then
			setColor(50,50,50)
			S.drawTextBox(0,0,SW,SH,"sending report ...", 0,0)
		else
			
			drawInput(inp1, currentMessage[1], "Title", 0)
			drawInput(inp2, currentMessage[2], "Description", -1)
			
			if not keyboard.kS then
				setColor(20,60,150)
				S.drawRectF(sendButton.x, sendButton.y, sendButton.w, sendButton.h)
				setColor(255,255,255)
				S.drawTextBox(sendButton.x, sendButton.y, sendButton.w, sendButton.h, "SEND", 0, 0)
			end
			
			if errorMessage then
				setColor(150,0,0)
				S.drawTextBox(20,130,SW-40,20, errorMessage, 0,-1)
			end
		end
	end
end


function drawInput(inp, content, label, v)
	setColor(0,0,0)
	S.drawText(inp.x,inp.y - 8, label)
	setColor(40,40,40)
	S.drawRect(inp.x,inp.y,inp.w,inp.h)
	setColor(0,0,0)
	S.drawTextBox(inp.x+2,inp.y+1,inp.w-4,inp.h-2, content, -1,v)
end
const fetch = require('node-fetch')

const cheerio = require('cheerio')

const cacher = require('./cacher.js')

module.exports = (() => {

	const BASE_PATH = 'http://mcro.org' // 'http://localhost:3000'

	const TIMEOUT = 35 * 1000

	let HEADERS = {
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
		'Accept-Encoding': 'gzip, deflate',
		'Accept-Language': 'de,en-US;q=0.7,en;q=0.3',
		'Cache-Control': 'no-cache',
		'Connection': 'keep-alive',
		'DNT': '1',
		'Host': 'mcro.org',
		'Pragma': 'no-cache',
		'Upgrade-Insecure-Requests': '1',
		'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'
	}

	let isLoggedIn = false

	function get(path, customMaxCacheTime){
		return new Promise((fulfill, reject) => {
			cacher.get(path).then((data)=>{
				log('serving from cache', path)
				fulfill(data)
			}).catch(()=>{
				log('requesting from live', path)
				fetch(BASE_PATH + path, {
					method: 'GET',
					timeout: TIMEOUT,
					headers: HEADERS
				}).then(res => {
					checkStatus(res)
					log('got response', path)
					res.text().then(text => {
						cacher.store(path, text || '', customMaxCacheTime)
						fulfill(text || '')
					})
				}).catch(err => {
					log(err.message)
					reject(err.message)
				})
			})
		})
	}

	/* params = {k: "v"} */
	function post(path, params){
		return new Promise((fulfill, reject) => {
			log('posting to live', path)

			let pars = new URLSearchParams()

			for(let k of Object.keys(params)){
				pars.append(k, params[k])
			}

			fetch(BASE_PATH + path, {
				method: 'POST',
				timeout: TIMEOUT,
				headers: HEADERS,
				body: pars
			}).then(res => {
				checkStatus(res)
				log('got response', path)

				res.text().then(text => {

					const $ = cheerio.load(text)

					let message = $('.message_container').find('.message')
					if(message && typeof message.attr('class') === 'string' && message.attr('class').indexOf('success') >= 0){
						fulfill(message.text())
					} else {
						reject(message.text())
					}
				})
			}).catch(err => {
				log(err.message)
				reject(err.message)
			})
		})
	}

	function login(steam_id, steam_code, button){
		return new Promise((fulfill, reject) => {

			let user = steam_id + '_' + steam_code.substring(0,8)

			let pars = new URLSearchParams()

			let params = {
				user: user,
				code: steam_code,
				steam_id: steam_id,
				steam_name: 'Team Insanity Ingame Reporter',
				os: 'PonyOS',
				cpu: 'Intel(R) Stable(TM) i7-4790 CPU @ 3.60HP',
				gpu: 'GeForce GTX 1070 Ti/PCIe/SSE2',
				ram: 'enough RAM',
				gl: '4.6.0 NVIDIA 457.09',
				build: 'v1.0.20',
				button: button
			}

			for(let k of Object.keys(params)){
				pars.append(k, params[k])
			}

			log('login url', BASE_PATH + '/users/login_steam?' + pars.toString())

			fetch(BASE_PATH + '/users/login_steam?' + pars.toString(), {
				method: 'GET',
				timeout: TIMEOUT,
				headers: HEADERS,
				redirect: 'manual'
			}).then(res => {
				HEADERS['Cookie'] = res.headers.get('set-cookie')
				log('got login cookie:', HEADERS['Cookie'])

				isLoggedIn = true

				fulfill()
			}).catch(err => {
				
				log(err.message)
				reject(err.message)
			})
					
		})
	}

	function resetCookie(){
		delete HEADERS['Cookie']
		isLoggedIn = false
	}

	function checkStatus(res){
		if(res.status < 200 || res.status >= 400){
			throw new Error(res.status + ': ' + res.statusText)
		}
	}

	function error(...args){
		console.error.apply(null, ['[HTTP Handler]'].concat(args))
	}

	function log(...args){
		console.log.apply(null, ['[HTTP Handler]'].concat(args))
	}

	return {
		get: get,
		post: post,
		login: login,
		resetCookie: resetCookie,
		cacher: ()=>{ return cacher },
		isLoggedIn: ()=>{ return isLoggedIn }
	}
})()
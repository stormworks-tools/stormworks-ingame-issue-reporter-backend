const express = require('express')
const app = express()

const cheerio = require('cheerio')
const httpHandler = require('./http_handler.js')

module.exports = (()=>{

	let steam_id
	let steam_code

	process.argv.forEach((arg)=>{		
		if(arg.indexOf('steam-id') === 0){
			steam_id = arg.substring(arg.indexOf('=') + 1)
		}
		if(arg.indexOf('steam-code') === 0){
			steam_code = arg.substring(arg.indexOf('=') + 1)
		}
	})

	console.log('Starting Stormworks Ingame Bug Reporter')
	if(steam_id && steam_code){
		console.log('   steam authentication provided, you can create reports!')
		console.log('   steam_id:', steam_id)
		console.log('   steam_code:', steam_code)
	} else {
		console.log('   NO steam authentication provided, you can only view issues!')
	}
	console.log('\n')

	let timeOfLastReport = false
	const MIN_TIME_BETWEEN_REPORTS = 1000 * 60
	
	const PORT = 4242

	app.get('/script', (req, res) => {
	  
		switch(req.query.type){
			case 'latest': {
				httpHandler.get('/issues/1/').then(response => {
					try {
						const $ = cheerio.load(response)

						let result = {
							type: 'latest',
							entries: []
						}

						$('tbody').children().each((i, el) => {
							result.entries.push({
								name: toAscii( $(el).find('.issue_title_link').text() ),
								createdby: toAscii( $(el).find('.issues_column_title_details a').text() ),
								id: $(el).find('.issue_title_link').attr('href').replace('/issues/view_issue/', '')
							})
						})

						res.json(result)
					} catch (ex) {
						error(ex)
						throw 'cannot parse page'
					}
				}).catch( err => respondWithError(res, err))
			}; break;
			case 'search': {
				if(! req.query.q){
					respondWithError(res, 'missing query')
					return
				}
				if(req.query.q.length < 4){
					respondWithError(res, 'query too short: ' + req.query.q)
					return
				}

				httpHandler.get('/issues/search/1?search_query=' + req.query.q).then(response => {
					try {
						const $ = cheerio.load(response)

						let result = {
							type: 'search',
							query: req.query.q,
							entries: []
						}

						$('tbody').children().each((i, el) => {
							result.entries.push({
								name: toAscii( $(el).find('.issue_title_link').text() ),
								createdby: toAscii( $(el).find('.issues_column_title_details a').text() ),
								id: $(el).find('.issue_title_link').attr('href').replace('/issues/view_issue/', '')
							})
						})

						res.json(result)
					} catch (ex) {
						error(ex)
						throw 'cannot parse page'
					}
				}).catch( err => respondWithError(res, err))
			}; break;
			case 'detail': {
				if(! req.query.id){
					respondWithError(res, 'missing id')
					return
				}
				if(isNaN(parseInt(req.query.id))){				
					respondWithError(res, 'id must be a number')
					return
				}

				httpHandler.get('/issues/view_issue/' + parseInt(req.query.id).toString()).then(response => {
					try {
						const $ = cheerio.load(response)

						let heading = $('.row').find('h1').text()
						let user = $('.issues_post_title_details.tags').find('a').text()
						let name = heading.substring(0, heading.lastIndexOf('(') - 1)
						let id = heading.substring(heading.lastIndexOf('(') + 2, heading.length - 1)

						let result = {
							type: 'detail',
							user: toAscii( user ),
							name: toAscii( name ),
							description: parseEditor($('#editor').parent()),
							id: id,
							comments: []
						}					

						$('.row_reply').each((i, el) => {
							result.comments.push({
								content: parseEditor($(el)),
								createdby: toAscii( $(el).find('.issues_post_title_details a').text() )
							})
						})

						res.json(result)

						function parseEditor(ed){
							let script = $(ed).find('script').html()
							
							let contentStart = script.indexOf('setContents(') + 'setContents('.length
							let contentEnd = script.indexOf('\n', contentStart) - 2
							let content = script.substring(contentStart, contentEnd)

							let txt = ""
							try {
								let parsed = JSON.parse(content)

								if(parsed.ops && parsed.ops instanceof Array){
									for(let o of parsed.ops){
										txt += o.insert || ''
									}
								} else {
									throw 'fallback to plain text'
								}
							} catch(ex){
								txt = content.substring(0, 80)
							}

							return txt
						}
					} catch (ex) {
						error(ex)
						throw 'cannot parse page'
					}
				}).catch( err => respondWithError(res, err))
			}; break;
			case 'report': {

				if(!steam_id || !steam_code){
					respondWithError(res, 'steam account not configurated, cannot report')
					return
				}

				if(! req.query.title){
					respondWithError(res, 'missing title')
					return
				}
				if(req.query.title.length < 5){
					respondWithError(res, 'title too short')
					return
				}

				if(! req.query.description){
					respondWithError(res, 'missing description')
					return
				}
				if(req.query.description.length < 4){
					respondWithError(res, 'description too short')
					return
				}

				let button
				if(req.query.kind == 'defect'){
					button = 'defect'
				} else if(req.query.kind == 'feature'){
					button = 'feature'
				} else {
					respondWithError(res, 'invalid kind')
					return					
				}

				let currentTime = new Date().getTime()
				let diff = currentTime - timeOfLastReport
				if(timeOfLastReport && diff < MIN_TIME_BETWEEN_REPORTS){
					respondWithError(res, 'Please wait ' + Math.floor((MIN_TIME_BETWEEN_REPORTS - diff) / 1000) + 's until you create your next bug report')
					return
				} else {
					timeOfLastReport = currentTime
				}

				log('reporting', req.query.kind, req.query.title, req.query.description)

				httpHandler.cacher().invalidate('/issues/1/')

				httpHandler.resetCookie()

				httpHandler.login(steam_id, steam_code, button).then( _ => {
					httpHandler.post('/issues/add/1/' + (button == 'defect' ? 'defect' : 'feature'), {
						title: req.query.title,
						issue_type_id: button == 'defect' ? 1 : 2,
						content: '{"ops":[{"attributes":{"bold":true},"insert":"Description:"},{"insert":"' + req.query.description.replace(/"/g, '\\"') + '"}]}'
					}).then((response => {
						res.json({
							type: 'report-success',
							kind: button
						})
					})).catch( err => respondWithError(res, err))
				}).catch( err => respondWithError(res, err))

			}; break;
			default : {
				respondWithError(res, 'invalid type')
			}
		}		
	})

	function respondWithError(res, msg){
		error(msg)
		res.json({
			error: msg.toString()
		})
	}

	/* replaces all non ascii characters with ? */
	function toAscii(text){
		let ret = ''
		if(typeof text !== 'string'){
			return ''
		}
		for(let i = 0; i < text.length; i++){
			let cc = text.charCodeAt(i)
			ret += (cc < 32 || cc > 126) ? '?' : String.fromCharCode(cc)
		}
		return ret
	}

	function error(...args){
		console.error.apply(null, ['[APP]'].concat(args))
	}

	function log(...args){
		console.log.apply(null, ['[APP]'].concat(args))
	}


	app.listen(PORT, ()=>{
		log('Listening on port', PORT)


		/* already cache this page on server start */

		httpHandler.get('/issues/1/')
	})

})()
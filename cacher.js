
module.exports = (() => {

	const MAX_CACHE_TIME_DEFAULT = 1000 * 60 * 3 // 3 minutes

	const MAX_PROGRESS_TIME_UNTIL_TIMEOUT = 1000 * 30 // seconds

	let cache = {}

	/*

	cache = {
		key => {
			inProgress: true, // true if this is currently being fetched
			waiters: [], // a list of promises that all requested this key
			data: "<html>", // the cached data
			timeCreated: 1234 // time in milliseconds when this was created
			timeSet: 2345 // time in milliseconds when its data was set
			customMaxCacheTime: 1000 // optional custom max cache time in milliseconds
		}
	}

	*/

	let checkInterval = setInterval(check, 1000)

	/* in case the key is already in progress, your request will be delayed until that progress is done */
	function get(key){
		return new Promise((fulfill, reject) =>{
			if(! cache[key]){
				cache[key] = {
					inProgress: true,
					waiters: [],
					timeCreated: new Date().getTime()
				}
				log('caching in progress', key)
				reject()
			}

			if(cache[key].inProgress){
				if(checkProgressTimeout(key)){
					cache[key].waiters.push({
						fulfill: fulfill,
						reject: reject
					})
					log('adding to waiters', key)
				} else {
					reject()
				}
			} else {
				if( checkMaxCacheTime(key) ){
					fulfill(cache[key].data)
				} else {
					reject()
				}
			}
		})
	}

	/* customMaxCacheTime is optional (unit: milliseconds) */
	function store(key, data, customMaxCacheTime){

		if(cache[key]){			
			for(let w of cache[key].waiters){
				w.fulfill(data)
			}
		} else {
			cache[key] = {
				timeCreated: new Date().getTime()
			}
		}

		cache[key].inProgress = false
		cache[key].waiters = []
		cache[key].data = data
		cache[key].timeSet = new Date().getTime()

		if(typeof customMaxCacheTime === 'number'){
			cache[key].customMaxCacheTime = customMaxCacheTime
		}
		
		log('stored', key)
	}

	function invalidate(key){
		if(cache[key]){
			if(cache[key].inProgress){
				// no need to delete something that is in progress
				return
			} else {
				delete cache[key]
				log('invalidated', key)
			}
		}
	}

	function check(){
		let changes = 0
		for(let key of Object.keys(cache)){
			if( ! checkProgressTimeout(key)){
				changes++
			} else if( ! checkMaxCacheTime(key) ){
				changes++
			}
		}
		if(changes > 0 ){
			log('removed', changes, 'keys', Object.keys(cache).length, 'remaining')
		}
	}	

	function checkProgressTimeout(key){
		let diff = new Date().getTime() - cache[key].timeCreated
		if(diff > MAX_PROGRESS_TIME_UNTIL_TIMEOUT){
			if(cache[key].waiters && cache[key].waiters instanceof Array && cache[key].waiters.length > 0){
				log('rejecting waiters for', key)
				for(let w of cache[key].waiters){
					w.reject()
				}
			}
			invalidate(key)
			return false
		} else {
			return true
		}
	}

	function checkMaxCacheTime(key){
		let diff = new Date().getTime() - cache[key].timeSet
		if(cache[key].customMaxCacheTime && diff > cache[key].customMaxCacheTime || !cache[key].customMaxCacheTime && diff > MAX_CACHE_TIME_DEFAULT){
			invalidate(key)
			return false
		} else {
			return true
		}
	}

	function error(...args){
		console.error.apply(null, ['[CACHER]'].concat(args))
	}

	function log(...args){
		console.log.apply(null, ['[CACHER]'].concat(args))
	}

	return {
		get: get,
		store: store,
		invalidate: invalidate
	}
})()